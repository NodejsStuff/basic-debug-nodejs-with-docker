FROM node:12.4.0-alpine as debug

WORKDIR /app
COPY ./package.json ./
RUN npm install --no-optional && npm cache clean --force
RUN npm install -g nodemon

COPY . .

ENTRYPOINT [ "npm","run","start" ]




