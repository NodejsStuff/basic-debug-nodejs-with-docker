'use strict';

const express = require('express');

const PORT = 5000;
const HOST = '0.0.0.0';

const app = express();

app.get('/', (req, res) => {
  res.send('Hello world \n' + req.url);
});


app.listen(PORT, HOST);

console.log(`Running version 15  on http://${HOST}:${PORT}`);

//docker build -t tests .
//docker run -it -p 8050:5000 -p 9229:9229 -v ${pwd}:/app -v /app/node_modules  tests